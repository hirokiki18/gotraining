package bubble
import(
	"fmt"
)


func DoBubble(){
	numset := []int{4, 3, 8, 5, 2, 10, 35, 1, 15, 14}
	
	for i:=0; i<len(numset)-1; i++{
		for j:=0; j<len(numset)-1; j++{
			if numset[j] > numset[j+1] {
				swap(j, numset)
			}
		}
		fmt.Println(numset)
	}
}

func swap(index int, numset []int) {
	temp := numset[index]
	numset[index] = numset[index+1]
	numset[index+1] = temp
}
