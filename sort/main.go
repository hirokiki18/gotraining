package main

import (
	"fmt"
	"./quick"
	"./merge"
	"./bubble"
)

func main(){
	fmt.Println("quick sort start")
	quick.DoQuick()
	fmt.Println("quick sort done")
	fmt.Println("------------------------------")
	fmt.Println("merge")
	merge.DoMerge()
	fmt.Println("merge sort done")
	fmt.Println("------------------------------")
	fmt.Println("bubble sort start")
	bubble.DoBubble()
	fmt.Println("------------------------------")
	fmt.Println("bubble sort done")
}


