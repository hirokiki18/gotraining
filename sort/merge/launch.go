package merge

import(
	"fmt"
)

func DoMerge() {
	origNumset := []int{4, 3, 8, 5, 2, 10, 35, 1, 15, 14, 9, 12}
	sortedNumset := mergeFunc(origNumset)
	fmt.Println(sortedNumset)
}

func mergeFunc(numset []int) []int{
	if len(numset) == 1 {
		return numset
	}
	half := len(numset)/2
	total := len(numset)
	leftNumset := numset[0:half]
	rightNumset := numset[half:total]

	mergedLeft := mergeFunc(leftNumset)
	mergedRight := mergeFunc(rightNumset)
	
	return merge(mergedLeft, mergedRight)
}



func merge(numset1 []int, numset2 []int) []int{
	lNumset := numset1
	rNumset := numset2
	mergedlen := len(lNumset) + len(rNumset)
	var toSortSlice []int
	for i:=0; i < mergedlen; i++{
		if len(rNumset) == 0 {
			lTopNum, lNewNumset := shift(lNumset)
			toSortSlice = append(toSortSlice, lTopNum)
			lNumset = lNewNumset
		}else if len(lNumset) == 0 {
			rTopNum, rNewNumset := shift(rNumset)
			toSortSlice = append(toSortSlice, rTopNum)
			rNumset = rNewNumset
		}else{
			if lNumset[0] < rNumset[0] {
				lTopNum, lNewNumset := shift(lNumset)
				toSortSlice = append(toSortSlice, lTopNum)
				lNumset = lNewNumset
			}else{
				rTopNum, rNewNumset := shift(rNumset)
				toSortSlice = append(toSortSlice, rTopNum)
				rNumset = rNewNumset
			}
		}
	}
	fmt.Println(toSortSlice)
	return toSortSlice
}




func shift(numset []int) (int, []int){
	top := numset[0]
	slice := numset[1:]
	return top, slice
}


