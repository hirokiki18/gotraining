package quick

import (
	"fmt"
	"strconv"
	"time"
	"math/rand"
)


//main func of quick sort
func DoQuick(){
	origNumset := []int{4, 3, 8, 5, 2, 10, 35, 1, 15, 14}
	//再帰的に行う処理
	doEachSort(origNumset)
	
	fmt.Println(origNumset)
}

func doEachSort(privNumset []int) {
	var noNumFlg bool
	var leftNumset []int
	var rightNumset []int
	if !(len(privNumset) == 0) {
		//pivotをきめる
		pIndex := decidePIndex(privNumset)
		fmt.Println("pivot:" + strconv.Itoa(privNumset[pIndex]))
		fmt.Println("部分ソート前")
		fmt.Println(privNumset)

		var leftOrRight string
		var leftStartIndex int = 0 
		var rightStartIndex int = len(privNumset)-1
		for i:=0; i < 100; i-- {
			//pivotの左側からswapする数字を1つ選出
			leftOrRight = "left"
			leftIndex := selectSwapIndex(leftOrRight, pIndex, leftStartIndex, privNumset)
			//pivotの右側から同様に選出
			leftOrRight = "right"
			rightIndex := selectSwapIndex(leftOrRight, pIndex, rightStartIndex, privNumset)

			
			//leftIndexとrightIndexが重なったら1回目のsort終了
			if leftIndex == rightIndex {
				pIndex = leftIndex
				break
			}

			//左右の選出した数字を交換する
			swap(leftIndex, rightIndex, privNumset)

			//pivotの数字も移動するので、移動後のpivotの場所を更新しておく
			if leftIndex == pIndex {
				pIndex = rightIndex
			}else if rightIndex == pIndex {
				pIndex = leftIndex
			}
			
			leftStartIndex = leftIndex 
			rightStartIndex = rightIndex 
			
		}

		//1回目ソート後のスライスを表示
		fmt.Println("部分ソート後")
		fmt.Println(privNumset)

		//pivotを境に左右にわける
		leftNumset = privNumset[:pIndex]
		rightNumset = privNumset[pIndex+1:]

	}else{
		noNumFlg = true
	}

	if (len(leftNumset) == 0 && len(rightNumset) == 0) || noNumFlg {
	}else{
		//	再帰処理
		doEachSort(leftNumset)
		doEachSort(rightNumset)
	}

}

func swap(lIndex int, rIndex int, privNumset []int){
	temp := privNumset[lIndex]
	privNumset[lIndex] = privNumset[rIndex]
	privNumset[rIndex] = temp
}

func selectSwapIndex(lr string, pIndex int, startIndex int, privNumset []int) int{
	pValue := privNumset[pIndex]
	//左右のブロックから該当する数字がなかった場合はpIndexをそのまま選出する
	var trgtIndex int = pIndex
	switch lr{
		case "left":
			decideglFunc := NewDecideglFuncLeft()
			for i := startIndex; i <= pIndex; i++ {
				isSwapTrgt := decideglFunc(pValue, privNumset[i])
				if isSwapTrgt {
					trgtIndex = i
					break
				}
			}

		case "right":
			decideglFunc := NewDecideglFuncRight()
			for i := startIndex; i >= pIndex; i-- {
				isSwapTrgt := decideglFunc(pValue, privNumset[i])
				if isSwapTrgt {
					trgtIndex = i
					break
				}
			}
	}
	return trgtIndex
}





//DicideglFuncの引数が(int,int)の場合（引数がint, []intの場合もありかも）

func NewDecideglFuncLeft() func(int, int) bool{
		return func(pValue int, eachNumInNumset int) bool {
			return pValue < eachNumInNumset
		}
}
func NewDecideglFuncRight() func(int, int) bool{
		return func(pValue int, eachNumInNumset int) bool {
			return pValue > eachNumInNumset
		}
}


func decidePIndex(privNumset []int) int{
	rand.Seed(time.Now().UnixNano())
	max := len(privNumset)
	var pivotIndex int
	if max == 0 {
		max = 1
	}else{
		pivotIndex = rand.Intn(max)
	}
	return pivotIndex
}


